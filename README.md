# Title detection

I tried five different models:
- Rule-based baseline
- Logistic regression
- MLP
- Transformer network
- LSTM

## Data analysis

I've done a basic analysis of the dataset. I mainly focus on investigating what texts
the dataset contains and how predictive individual features are. Please
take a look at `analysis.ipynb`.

## Models

### Rule-based baseline

I just "trained" a simple threshold for the bounding box size. The implementation
is in `box_size.py`. You can train it using:
```bash
python train_model.py configs/box_size.yaml
```
You have to specify the location of data in the yaml config file.

### Logistic Regression

I trained a logistic regression model using these binary features:
- Is  the text bold
- Is the text in italic
- Is the test underlined
- Is the text in upper case.
 
and these numeric features:
- left coordinate
- right coordinate
- top coordinate
- bottom coordinate
- text length
- bounding box size
- number of words in the text

You can train the model using:
```bash
python train_model.py configs/log_reg.yaml
```
The script also does some gridsearch with k-fold cross-validation.
Numeric features were normalized. The main implementation is in `log_reg.py`.

### MLP

I trained a multilayer perceptron on the same data as I trained logistic regression.
The final MLP has two hidden layers. You can run it using:
```bash
python train_model.py configs/mlp.yaml
```
The main implementation is in `mlp.py`.

### Transformer

I decided to train a character level transformer since I felt like word based model would not
perform well on this data. By examining the data, we can notice that many of the texts
are not even proper texts. They are just something random extracted using OCR.

I also decided to do some basic feature engineering for characters.
For each character I computed these features:
- Is it a letter
- Is it an alphanumeric symbol
- Is it a number
- Is it upper case letter

The model runs couple transformer layers over the text and then averages over the last layer.
This output is then concatenated with the features that I used to train logistic regression.
We run simple MLP on the top of this concatenated vector.
You can run the training pipeline using:
```bash
python train_model.py configs/transformer.yaml
```
The main implementation is in `transformer.py`.

I used Tesla T4 to train the model. It takes around 20 sec. to train it. 

### LSTM

I also tried a simple LSTM model where we take the last output of LSTM and
concatenate it with the feature vector, and run MLP on the top of it.
Training of this model was super slow, so I abandoned this idea.

## Results

| model    | accuracy | F1    |
|----------|----------|--------
| box_size | 0.73     | 0.64  |
| log_reg  | 0.975    | 0.983 |
| MLP      | 0.981    | 0.964 |
| trans.   | 0.977    | 0.955 |

## Conclusion

When we use accuracy as a metric then MLP is the best model.
If we use F1 score then logistic regression is the best one.
I feel like F1 score is a better metric in this case since the classes are so
imbalanced (74 % negative class).
It is quite unfortunate that transformer model did not outperform MLP or logistic regression
significantly. This shows that the text extracted by using OCR is not that useful
and it's quite noisy. Probably the best features are the box size and the length of the text.
We can notice by manually examining the failed examples that indeed there's a lot of noise
in the data.

## Future Work

If I had more time I would also try word level transformer but I don't feel like
it would give that much improvement. Another thing that I would try would be a CNN model.
It might be quite fast to train and it might learn something useful on the top of character features.