import os
import datetime as dt

from dataset import Doc


def now_to_str():
    return dt.datetime.now().strftime("%Y%m%d%H%M%S")


def get_exp_path_prefix(config: dict):
    """Returns path prefix of an experiment."""
    artifact_dir = config["artifact_dir"]
    exp_name = config["exp_name"]
    res = f"{artifact_dir}/{exp_name}/{now_to_str()}"
    os.makedirs(res, exist_ok=True)

    return res


def box_size(doc: Doc) -> float:
    return (doc.right - doc.left) * (doc.bottom - doc.top)
