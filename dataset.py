import csv
import dataclasses

import typing as ty


@dataclasses.dataclass
class Doc:
    text: str
    is_bold: bool
    is_italic: bool
    is_underlined: bool
    left: float
    right: float
    top: float
    bottom: float
    font: str
    label: int

    @staticmethod
    def from_dict(obj: dict) -> 'Doc':
        return Doc(
            obj["Text"],
            obj["IsBold"] == "TRUE",
            obj["IsItalic"] == "TRUE",
            obj["IsUnderlined"] == "TRUE",
            float(obj["Left"]),
            float(obj["Right"]),
            float(obj["Top"]),
            float(obj["Bottom"]),
            obj["FontType"],
            int(obj["Label"]),
        )


class Dataset:

    def __init__(self, path: str):
        self._docs: ty.List[Doc] = []
        with open(path, encoding="cp1252") as f:
            reader = csv.DictReader(f)
            for row in reader:
                self._docs.append(Doc.from_dict(row))

    def iter(self):
        yield from (doc for doc in self._docs)

    def size(self):
        return len(self._docs)
