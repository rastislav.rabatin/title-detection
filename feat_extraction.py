import common
import numpy as np

from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler
from dataset import Dataset


def get_feat_extractors():
    return [
        # Binary features.
        lambda x: x.is_bold,
        lambda x: x.is_italic,
        lambda x: x.is_underlined,
        lambda x: x.text.isupper(),

        # Numeric features
        lambda x: x.left,
        lambda x: x.right,
        lambda x: x.top,
        lambda x: x.bottom,
        lambda x: x.bottom,
        lambda x: len(x.text),
        lambda x: common.box_size(x),
        lambda x: len(x.text.split()),
    ]


def compute_feats(dataset: Dataset):
    feats = np.zeros((dataset.size(), len(get_feat_extractors())))
    for i, doc in enumerate(dataset.iter()):
        for j, fn in enumerate(get_feat_extractors()):
            feats[i, j] = fn(doc)

    return feats


def get_feat_transformer():
    return ColumnTransformer(
        [
            ("pass", "passthrough", list(range(4))),
            ("norm", StandardScaler(), list(range(4, len(get_feat_extractors())))),
        ]
    )
