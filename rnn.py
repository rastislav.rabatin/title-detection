import tensorflow as tf
import typing as ty
import numpy as np
from tensorflow.python.keras.metrics import Precision, Recall

import common
from classifier import Classifier
from dataset import Dataset
from tensorflow.keras.layers import Input, Embedding, Concatenate, LSTM, Bidirectional, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.models import load_model

from dnn import compute_inputs, save_model_structure, save_model_summary, N_CHAR_FEATS
from feat_extraction import get_feat_extractors


class Rnn(Classifier):
    """Binary classifier interface."""

    def __init__(self, config: ty.Optional[dict]):
        """Compile model from a config. Returns non-trained model."""
        gpus = tf.config.experimental.list_physical_devices('GPU')
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)

        embedding_size = config["embedding_size"]
        n_chars = config["n_chars"]
        lstm_dims = config["lstm_dims"]
        n_lstm_layers = config["n_lstm_layers"]
        ffn_dims = config["ffn_dims"]
        n_ffn = config["n_ffn"]

        self._config = config

        chars_input = Input(shape=(None,), dtype=tf.int32, ragged=True)
        char_feats = Input(shape=(None, N_CHAR_FEATS), dtype=tf.int32, ragged=True)
        other_feats = Input(shape=(len(get_feat_extractors()),), dtype=tf.float32)

        # n_chars plus out of vocabulary char.
        x = Embedding(n_chars + 1, embedding_size)(chars_input)
        x = Concatenate(axis=-1)([x, tf.cast(char_feats, tf.float32)])

        for i in range(n_lstm_layers - 1):
            x = Bidirectional(LSTM(units=lstm_dims, return_sequences=True))(x)

        x = Bidirectional(LSTM(units=lstm_dims, return_sequences=False))(x)
        x = Concatenate(axis=-1)([x, other_feats])

        for _ in range(n_ffn - 1):
            x = Dense(units=ffn_dims)(x)

        output = Dense(units=1, activation="sigmoid")(x)
        model = Model(inputs=[chars_input, char_feats, other_feats], outputs=output)
        model.compile("adam", "binary_crossentropy",
                      metrics=["accuracy", Precision(), Recall()])
        self._model = model

        path_prefix = config["path_prefix"]
        save_model_structure(model, path_prefix)
        save_model_summary(model, path_prefix)

    def fit(self, dataset: Dataset):
        """Train the model on a given dataset"""
        n_chars = self._config["n_chars"]
        chars, char_feats, other_feats = compute_inputs(dataset, n_chars)
        self._model.fit(
            x=[chars, char_feats, other_feats],
            y=np.array([doc.label for doc in dataset.iter()]),
            epochs=self._config["n_epochs"],
            verbose=True,
            batch_size=self._config["batch_size"]
        )

    def predict_proba(self, dataset: Dataset) -> np.array:
        """Predict probability for each sample in the dataset"""
        n_chars = self._config["n_chars"]
        chars, char_feats, other_feats = compute_inputs(dataset, n_chars)
        res = self._model.predict([chars, char_feats, other_feats])
        res = np.concatenate([1 - res, res], axis=1)
        return res

    def save(self, path_prefix: str):
        """Serialize the model to file"""
        self._model.save(f"{path_prefix}/model.h5")

    @classmethod
    def load(cls, path_prefix: str, config: dict) -> Classifier:
        """Deserialize trained model."""
        res = Rnn(config)
        res._model = load_model(f"{path_prefix}/model.h5")
        return res
