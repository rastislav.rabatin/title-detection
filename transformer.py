import tensorflow as tf
import typing as ty
import numpy as np
from keras.layers import Dropout
from tensorflow.python.keras import Sequential
from tensorflow.python.keras.engine.base_layer import Layer
from tensorflow.python.keras.layers import MultiHeadAttention, LayerNormalization, Embedding, GlobalAveragePooling1D
from tensorflow.python.keras.metrics import Precision, Recall

from callbacks import get_callbacks
from classifier import Classifier
from dataset import Dataset
from tensorflow.keras.layers import Input, Concatenate, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.models import load_model

from dnn import compute_inputs, save_model_structure, save_model_summary, N_CHAR_FEATS
from feat_extraction import get_feat_extractors


class TransformerBlock(Layer):
    def __init__(self, embed_dim, num_heads, ff_dim, rate=0.1):
        super(TransformerBlock, self).__init__()

        self._config = {
            "embed_dim": embed_dim,
            "num_heads": num_heads,
            "ff_dim": ff_dim,
            "rate": rate,
        }

        self.att = MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim)
        self.ffn = Sequential(
            [Dense(ff_dim, activation="relu"), Dense(embed_dim)]
        )
        self.layernorm1 = LayerNormalization(epsilon=1e-6)
        self.layernorm2 = LayerNormalization(epsilon=1e-6)
        self.dropout1 = Dropout(rate)
        self.dropout2 = Dropout(rate)

    def call(self, inputs, training):
        attn_output = self.att(inputs, inputs)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(inputs + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=training)
        return self.layernorm2(out1 + ffn_output)

    def get_config(self):
        config = super().get_config().copy()
        config.update(self._config)
        return config


class TokenAndPositionEmbedding(Layer):
    def __init__(self, max_len, vocab_size, embed_dim):
        super(TokenAndPositionEmbedding, self).__init__()
        self._config = {
            "max_len": max_len,
            "vocab_size": vocab_size,
            "embed_dim": embed_dim,
        }
        self.token_emb = Embedding(input_dim=vocab_size, output_dim=embed_dim)
        self.pos_emb = Embedding(input_dim=max_len, output_dim=embed_dim)

    def call(self, x):
        maxlen = tf.shape(x)[-1]
        positions = tf.range(start=0, limit=maxlen, delta=1)
        positions = self.pos_emb(positions)
        x = self.token_emb(x)
        return x + positions

    def get_config(self):
        config = super().get_config().copy()
        config.update(self._config)
        return config


class Transformer(Classifier):
    """Binary classifier interface."""

    def __init__(self, config: ty.Optional[dict]):
        """Compile model from a config. Returns non-trained model."""
        gpus = tf.config.experimental.list_physical_devices('GPU')
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)

        embedding_size = config["embedding_size"]
        n_chars = config["n_chars"]
        ffn_dims = config["ffn_dims"]
        n_ffn = config["n_ffn"]
        max_len = config["max_len"]
        trans_dim = config["trans_dim"]
        num_heads = config["num_heads"]
        n_trans = config["n_trans"]
        dropout = config.get("dropout", 0.1)

        self._config = config

        n_other_feats = len(get_feat_extractors())
        chars_input = Input(shape=(max_len,), dtype=tf.int32)
        char_feats = Input(shape=(max_len, N_CHAR_FEATS), dtype=tf.int32)
        other_feats = Input(shape=(n_other_feats,), dtype=tf.float32)

        # n_chars plus out of vocabulary char plus padding character
        x = TokenAndPositionEmbedding(max_len, n_chars + 2, embedding_size)(chars_input)
        x = Concatenate(axis=-1)([x, tf.cast(char_feats, tf.float32)])

        for i in range(n_trans):
            x = TransformerBlock(embedding_size + N_CHAR_FEATS,
                                 num_heads, trans_dim, dropout)(x)

        x = GlobalAveragePooling1D()(x)
        x = Concatenate(axis=-1)([x, other_feats])

        for i in range(n_ffn):
            x = Dense(units=ffn_dims / 2**i, activation="relu")(x)
            x = Dropout(config["dropout"])(x)

        x = Dense(units=ffn_dims / 2**n_ffn, activation="relu")(x)
        output = Dense(units=1, activation="sigmoid")(x)
        model = Model(inputs=[chars_input, char_feats, other_feats], outputs=output)

        def get_lr_metric(optimizer):
            def lr(y_true, y_pred):
                return optimizer.lr
            return lr

        opt = tf.keras.optimizers.Adam(learning_rate=config["init_lr"])
        lr_metric = get_lr_metric(opt)
        model.compile(opt, "binary_crossentropy",
                      metrics=["accuracy", Precision(), Recall(), lr_metric])
        self._model = model

        path_prefix = self._config["path_prefix"]
        save_model_structure(model, path_prefix)
        save_model_summary(model, path_prefix)

    def fit(self, dataset: Dataset):
        """Train the model on a given dataset"""
        n_chars = self._config["n_chars"]
        chars, char_feats, other_feats = compute_inputs(
            dataset, n_chars, self._config["max_len"])
        self._model.fit(
            x=[chars, char_feats, other_feats],
            y=np.array([doc.label for doc in dataset.iter()]),
            epochs=self._config["n_epochs"],
            verbose=True,
            shuffle=True,
            batch_size=self._config["batch_size"],
            callbacks=get_callbacks(self._config),
            validation_split=0.2,
        )

    def predict_proba(self, dataset: Dataset) -> np.array:
        """Predict probability for each sample in the dataset"""
        n_chars = self._config["n_chars"]
        chars, char_feats, other_feats = compute_inputs(
            dataset, n_chars, self._config["max_len"])
        res = self._model.predict([chars, char_feats, other_feats])
        res = np.concatenate([1 - res, res], axis=1)
        return res

    def save(self, path_prefix: str):
        """Serialize the model to file"""
        self._model.save(f"{path_prefix}/model.h5")

    @classmethod
    def load(cls, path_prefix: str, config: dict) -> Classifier:
        """Deserialize trained model."""
        res = Transformer(config)
        res._model = load_model(f"{path_prefix}/model.h5")
        return res
