"""Script for training classifier on a dataset."""
import argparse
import json
import time
import yaml
import importlib
import numpy as np
import logger
import common

from shutil import copyfile
from classifier import Classifier
from dataset import Dataset
from sklearn.metrics import classification_report


def _load_untrained_model(config: dict):
    logger.info("Loading untrained model...")
    model_module = importlib.import_module(config["model_module"])
    model_class = getattr(model_module, config["model_class"])
    return model_class(config)


def _eval_model(model: Classifier, dataset: Dataset):
    pred_probas = model.predict_proba(dataset)
    pred = np.argmax(pred_probas, axis=1)
    labels = [x.label for x in dataset.iter()]
    return classification_report(labels, pred, output_dict=True,
                                 target_names=["not_title", "title"])


def _get_wrong(model: Classifier, dataset: Dataset):
    pred_probas = model.predict_proba(dataset)
    pred = np.argmax(pred_probas, axis=1)
    labels = [x.label for x in dataset.iter()]
    return [
        (doc.text, doc.label)
        for doc, pred in zip(dataset.iter(), pred)
        if doc.label != pred
    ]


def _main():
    parser = argparse.ArgumentParser()
    parser.add_argument("exp_config", help="Path to experiment config")
    parser.add_argument("--log_stderr", help="Log to stderr",
                        action="store_true", required=False, default=False)
    parser.add_argument("--log_level", required=False, default="INFO")
    args = parser.parse_args()

    config = yaml.safe_load(open(args.exp_config))
    train_path = config["train_path"]
    test_path = config["test_path"]

    path_prefix = common.get_exp_path_prefix(config)
    config["path_prefix"] = path_prefix
    copyfile(args.exp_config, f"{path_prefix}/config.yaml")

    if args.log_stderr:
        logger.init()
    else:
        logger.init(filename=f"{path_prefix}/training.log")

    logger.info(f"exp_path={path_prefix}")
    logger.info("config", config=config)
    model: Classifier = _load_untrained_model(config)

    logger.info("Parsing dataset...")
    start = time.time()
    train_set = Dataset(train_path)
    logger.info(f"Read {train_set.size()} samples. ",
                duration=time.time() - start)

    logger.info("Training model...")
    start = time.time()
    model.fit(train_set)
    logger.info(f"Finished training: {time.time() - start} sec")
    model.save(path_prefix)
    logger.info("Saved model")

    logger.info("Evaluating model...")
    start = time.time()
    test_set = Dataset(test_path)
    test_report = _eval_model(model, test_set)
    train_report = _eval_model(model, train_set)
    logger.info(f"Finished evaluation: {time.time() - start} sec")

    wrong = _get_wrong(model, train_set)
    wrong_path = f"{path_prefix}/wrong.csv"
    with open(wrong_path, "w+") as f:
        f.write("\n".join(f'"{t}",{l}' for t, l in wrong))

    logger.info("Saving misclassified examples", path=wrong_path)

    logger.info("Writing classification report")
    with open(f"{path_prefix}/report.txt", "w+") as f:
        json.dump(train_report, f, indent=2)
        f.write("\n")
        json.dump(test_report, f, indent=2)
    logger.info("Finished writing classification report")


if __name__ == "__main__":
    _main()
