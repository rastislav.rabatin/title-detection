import unittest
import dataset as module


class DatasetTest(unittest.TestCase):

    def test_load(self):
        dataset = module.Dataset("data/sample.csv")
        self.assertEqual(19, dataset.size())


if __name__ == "__main__":
    unittest.main()
