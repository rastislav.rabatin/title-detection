from __future__ import annotations

import abc
import typing as ty
import numpy as np

from dataset import Dataset


class Classifier(abc.ABC):
    """Binary classifier interface."""

    @abc.abstractmethod
    def __init__(self, config: ty.Optional[dict]):
        """Compile model from a config. Returns non-trained model."""

    @abc.abstractmethod
    def fit(self, dataset: Dataset):
        """Train the model on a given dataset"""

    @abc.abstractmethod
    def predict_proba(self, dataset: Dataset) -> np.array:
        """Predict probability for each sample in the dataset"""

    @abc.abstractmethod
    def save(self, path_prefix: str):
        """Serialize the model to file"""

    @classmethod
    @abc.abstractmethod
    def load(cls, path_prefix: str, config: dict) -> Classifier:
        """Deserialize trained model."""


