import typing as ty
import numpy as np
import common

from dataset import Dataset
from classifier import Classifier


class BoxSizeClassifier(Classifier):
    """Binary classifier interface."""

    def __init__(self, config: ty.Optional[dict]):
        """Compile model from a config. Returns non-trained model."""
        self._best_threshold: float = -1.

    def fit(self, dataset: Dataset):
        """Train the model on a given dataset"""
        size_label = sorted(
            (common.box_size(doc), doc.label) for doc in dataset.iter()
        )
        zeros_left = 0
        ones_right = sum(label == 1 for _, label in size_label)
        best_misclassified = ones_right
        for size, label in size_label:
            if label == 0:
                zeros_left += 1
            else:
                ones_right -= 1

            if zeros_left + ones_right < best_misclassified:
                self._best_threshold = size

    def predict_proba(self, dataset: Dataset) -> np.array:
        """Predict probability for each sample in the dataset"""
        res = np.zeros((dataset.size(), 2))
        for idx, doc in enumerate(dataset.iter()):
            is_title = common.box_size(doc) <= self._best_threshold
            res[idx, 1] = is_title
            res[idx, 0] = not is_title

        return res

    def save(self, path_prefix: str):
        """Serialize the model to file"""
        with open(f"{path_prefix}/box_size.txt", "w+") as f:
            f.write(str(self._best_threshold))

    @classmethod
    def load(cls, path_prefix: str, config: dict) -> Classifier:
        """Deserialize trained model."""
        res = Classifier(config)
        with open(f"{path_prefix}/box_size.txt", "r") as f:
            line = f.read()
            res._best_threshold = float(line)

        return res
