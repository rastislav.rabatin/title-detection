"""A thin wrapper around standard logging library using structlog."""
import os
import sys

import structlog
import logging
import typing as ty


_LOGGER_NAME = "root"
_logger = None


def init(filename: ty.Optional[str] = None):
    logger = logging.getLogger(_LOGGER_NAME)
    logger.setLevel(os.environ.get("LOG_LEVEL", logging.INFO))

    if filename:
        log_handler = logging.FileHandler(filename=filename)
    else:
        log_handler = logging.StreamHandler(stream=sys.stderr)
    log_handler.setFormatter(logging.Formatter('%(message)s'))

    # Remove any existing handlers.
    for handler in logger.handlers[:]:
        logger.removeHandler(handler)

    logger.addHandler(log_handler)

    # do not send messages to root logger
    # https://docs.python.org/3/library/logging.html#logging.Logger.propagate
    logger.propagate = False

    structlog.configure(
        processors=[
            structlog.stdlib.add_logger_name,
            structlog.stdlib.add_log_level,
            structlog.stdlib.PositionalArgumentsFormatter(),
            structlog.processors.TimeStamper(fmt="iso"),
            structlog.processors.StackInfoRenderer(),
            structlog.processors.format_exc_info,
            structlog.dev.ConsoleRenderer(),
        ],
        context_class=structlog.threadlocal.wrap_dict(dict),
        logger_factory=lambda name: logging.getLogger(name),
        wrapper_class=structlog.stdlib.BoundLogger,
        cache_logger_on_first_use=True,
    )

    global _logger
    _logger = structlog.get_logger(_LOGGER_NAME).new()


def info(*args, **kwargs):
    """Log message with level INFO."""
    return _logger.info(*args, **kwargs)


def debug(*args, **kwargs):
    """Log message with level DEBUG."""
    return _logger.debug(*args, **kwargs)


def warn(*args, **kwargs):
    """Log message with level WARNING."""
    return _logger.warn(*args, **kwargs)


def warning(*args, **kwargs):
    """Log message with level WARNING."""
    return _logger.warning(*args, **kwargs)


def error(*args, **kwargs):
    """Log message with level ERROR."""
    return _logger.error(*args, **kwargs)


def exception(*args, **kwargs):
    """Log message with level ERROR."""
    return _logger.exception(*args, **kwargs)


def critical(*args, **kwargs):
    """Log message with level CRITICAL."""
    return _logger.critical(*args, **kwargs)


def setLevel(level):  # noqa N802
    """Set the minimum level of messages to log."""
    return _logger.setLevel(level)
