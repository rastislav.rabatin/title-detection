import os
import time

from keras.callbacks import ModelCheckpoint
from tensorflow.python.keras.callbacks import Callback, CSVLogger, LearningRateScheduler
from tensorflow.python.keras.optimizer_v2.learning_rate_schedule import CosineDecay

import logger


def get_callbacks(config):
    """Return keras callbacks"""
    path_prefix = config["path_prefix"]

    schedule = CosineDecay(config["init_lr"], config["lr_decay_steps"],
                           config["lr_alpha"])
    return [
        # get_checkpointer(path_prefix),
        EpochTime(),
        CSVLogger(f"{path_prefix}/epochs.csv", append=True),
        LearningRateScheduler(schedule=schedule),
    ]


def log_learning_rate(fn):
    def wrapper(*args, **kwargs):
        new_lr, epoch = fn(*args, **kwargs)
        logger.info(f"Epoch {epoch + 1}: Setting learning rate to {new_lr}")

        return new_lr

    return wrapper


class EpochTime(Callback):
    def __init__(self):
        super(EpochTime, self).__init__()
        self.epoch_time_start = time.time()

    def on_epoch_begin(self, epoch, logs=None):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}

        cur_time = time.localtime()
        logs["epoch_start"] = time.strftime(
            "%Y-%m-%d-%H-%M-%S", time.localtime(self.epoch_time_start))
        logs["epoch_end"] = time.strftime("%Y-%m-%d-%H-%M-%S", cur_time)
        logs["epoch_time"] = time.mktime(cur_time) - self.epoch_time_start


def get_checkpointer(model_dir):
    os.makedirs(model_dir + "/checkpoints", exist_ok=True)
    filepath = model_dir + "/checkpoints/epoch_{epoch:05d}_loss_{loss:.4f}_" \
                           "val_loss_{val_loss:.4f}.hdf5"

    return ModelCheckpoint(verbose=True, monitor="val_loss", filepath=filepath)


