"""Multilayer perceptron."""
from __future__ import annotations

import joblib
import typing as ty
import numpy as np

from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.metrics import f1_score, make_scorer

from dataset import Dataset
from classifier import Classifier
from feat_extraction import get_feat_extractors, compute_feats, get_feat_transformer


class Mlp(Classifier):

    def __init__(self, config: ty.Optional[dict]):
        self._config = config
        self._feature_fns = get_feat_extractors()
        ct = get_feat_transformer()
        mlp = MLPClassifier(learning_rate="adaptive")
        pipe = Pipeline(steps=[("norm", ct), ("mlp", mlp)])
        self._model = GridSearchCV(
            pipe, config["grid_params"],
            scoring=make_scorer(f1_score),
            refit=True, cv=config["n_folds"],
            n_jobs=int(config["n_jobs"]),
            verbose=4,
        )

    def fit(self, dataset: Dataset):
        feats = compute_feats(dataset)
        labels = [x.label for x in dataset.iter()]
        self._model.fit(feats, labels)

    def predict_proba(self, dataset: Dataset) -> np.array:
        return self._model.predict_proba(compute_feats(dataset))

    def save(self, path_prefix: str):
        joblib.dump(self._model, f"{path_prefix}/model.joblib")

    @classmethod
    def load(cls, path_prefix, config: dict):
        res = cls(config)
        res._model = joblib.load(f"{path_prefix}/model.joblib")
        return res
