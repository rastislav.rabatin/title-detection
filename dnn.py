from collections import defaultdict

import numpy as np
import tensorflow as tf
from tensorflow.python.keras.utils.vis_utils import plot_model

import typing as ty
import logger
from dataset import Dataset
from feat_extraction import compute_feats, get_feat_transformer


N_CHAR_FEATS = 4


def get_char_cnts(dataset: Dataset):
    chars_cnt = defaultdict(int)
    for doc in dataset.iter():
        for c in doc.text:
            chars_cnt[c] += 1

    return sorted([(cnt, c) for c, cnt in chars_cnt.items()], reverse=True)


def compute_inputs(dataset: Dataset, n_chars: int, max_len: ty.Optional[int] = None):
    chars_cnt = get_char_cnts(dataset)
    # Use top self._n_chars. Characters with lower frequency are gonna be used
    # as out of vocabulary character.
    char_to_idx = {
        c: idx for idx, (cnt, c) in enumerate(chars_cnt[:n_chars])
    }
    chars = []
    char_feats = []
    for doc in dataset.iter():
        text = doc.text

        padding = 0
        if max_len:
            text = text[:max_len]
            padding = max_len - len(text)

        chars.append([
            char_to_idx[char] if char in char_to_idx else n_chars
            for char in text
        ] + [n_chars + 1 for _ in range(padding)])
        char_feats.append([
            [char.isalpha(), char.isalnum(), char.isnumeric(), char.isupper()]
            for char in text
        ] + [[0, 0, 0, 0] for _ in range(padding)])

    feats = compute_feats(dataset)
    ct = get_feat_transformer()
    other_feats = ct.fit_transform(feats)

    chars = np.asarray(chars)
    char_feats = np.array(char_feats)

    if not max_len:
        chars = tf.ragged.constant(chars)
        char_feats = tf.ragged.constant(char_feats)

    return chars, char_feats, other_feats


def save_model_structure(model, output_dir):
    model_json = model.to_json()
    with open(output_dir + "/model.json", "w+") as json_file:
        json_file.write(model_json)

    try:
        plot_model(model, output_dir + "/model.png", show_shapes=True)
    except ImportError:
        msg = "Failed to create graph of the model: pydot and/or graphviz is " \
              "missing"
        logger.warning(msg)
        print(msg, flush=True)


def save_model_summary(model, output_dir):
    with open(output_dir + "/model_summary.txt", "w+") as f:
        model.summary(print_fn=lambda line: f.write(line + "\n"))
